﻿namespace DIP
{
    public class TaskManager
    {
        public bool Run(ITask task)
        {
            //LOGIC 1

            task.Run();

            //LOGIC 3

            return false;
        }
    }
}
