﻿namespace LSP
{
    using System.Collections.Generic;

    public class Facebook : ISocialMedia, IShareable
    {
        public string Name { get; set; } = "Facebook";

        public List<string> GetAllPosts()
        {
            return new List<string>
            {
                "Facebook Post 1", "Facebook Post 2", "Facebook Post 3"
            };
        }

        public bool Share()
        {
            //LOGIC 

            return true;
        }
    }
}
