﻿namespace LSP
{
    using System.Linq;
    using System.Collections.Generic;

    public static class SocialMediaProvider
    {
        public static List<ISocialMedia> GetSocialMediaServices()
        {
            return new List<ISocialMedia> {
                new Facebook(), new Twitter()
            };
        }

        public static List<IShareable> GetShareableSocialMediaServices()
        {
            var socilMediaServices = GetSocialMediaServices();

            return socilMediaServices.OfType<IShareable>().ToList();
        }
    }
}
