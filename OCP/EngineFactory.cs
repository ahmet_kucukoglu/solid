﻿using System;

namespace OCP
{
    public static class EngineFactory
    {
        public static ISearchEngine CreateEngine(string engineName)
        {
            var engineNamespace = $"OCP.Engines.{engineName}Engine, OCP";
            var engineType = Type.GetType(engineNamespace);
            var engineInstance = (ISearchEngine)Activator.CreateInstance(engineType);

            return engineInstance;
        }
    }
}
