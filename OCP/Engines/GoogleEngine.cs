﻿namespace OCP.Engines
{
    using System;

    public class GoogleEngine : ISearchEngine
    {
        public string Search(string keyword)
        {
            return $"Search by Google for {keyword}";
        }
    }
}
