﻿namespace OCP.Engines
{
    public class YahooEngine : ISearchEngine
    {
        public string Search(string keyword)
        {
            return $"Search by Yahoo for {keyword}";
        }
    }
}
