﻿namespace OCP
{
    public interface ISearchEngine
    {
        string Search(string keyword);
    }
}
