﻿namespace SRP
{
    public class UserService
    {
        public void CreateUser(string emailAddress, string password)
        {
            var emailService = new EmailService();

            emailService.Validate(emailAddress);

            // CREATE LOGIC

            emailService.SendEmail(emailAddress);
        }
    }
}
